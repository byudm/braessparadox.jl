module Generators

import Random: seed!, MersenneTwister

import LightGraphs: # Types
                    SimpleGraph, AbstractGraph,
                    # Modifiers
                    add_edge!,
                    # Matrices
                    adjacency_matrix,
                    # Invariants
                    degree

export rand_tree, tree, barbell, conjoined_cycle

include("trees.jl")
include("barbells.jl")
include("cycles.jl")

end # module
